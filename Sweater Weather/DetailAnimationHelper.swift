//
//  DetailAnimationHelper.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 11/10/15.
//  Copyright © 2015 Tetragon. All rights reserved.
//

import Foundation
import UIKit

extension DetailViewController {
    
    // MARK: Top Temps
    func animateTopTemps() {
        topTempHigh.alpha = 0.0
        topTempLow.alpha = 0.0
        topHighLabel.alpha = 0.0
        topLowLabel.alpha = 0.0
        
        /* High Temp */
        UIView.animateWithDuration(0.8, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.topTempHigh.alpha = 1.0
                self.topHighLabel.alpha = 1.0
            },
            completion: nil)
        UIView.animateWithDuration(0.8, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.topTempHigh.center.y = self.topTempHigh.center.y - 50
                self.topHighLabel.center.y = self.topHighLabel.center.y - 50
            },
            completion: nil)
        
        /* Low Temp */
        UIView.animateWithDuration(0.8, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.topTempLow.alpha = 1.0
                self.topLowLabel.alpha = 1.0
            },
            completion: nil)
        UIView.animateWithDuration(0.8, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.topTempLow.center.y = self.topTempLow.center.y + 50
                self.topLowLabel.center.y = self.topLowLabel.center.y + 50
            },
            completion: nil)
    }
    
    // MARK: Lines
    func animateLines() {
        lineOne.alpha = 0.0
        lineTwo.alpha = 0.0
        lineThree.alpha = 0.0
        lineFour.alpha = 0.0
        lineFive.alpha = 0.0
        
        /* Line One */
        UIView.animateWithDuration(0.8, delay: 0.2, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.lineOne.alpha = 1.0
            },
            completion: nil)
        
        /* Line Two */
        UIView.animateWithDuration(0.8, delay: 0.3, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.lineTwo.alpha = 1.0
            },
            completion: nil)
        
        /* Line Three */
        UIView.animateWithDuration(0.8, delay: 0.4, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.lineThree.alpha = 1.0
            },
            completion: nil)
        
        /* Line Four */
        
        // Hide Fourth For iPhone 4 //
        if !DeviceType.IS_IPHONE_4_OR_LESS {
            UIView.animateWithDuration(0.8, delay: 0.5, options: UIViewAnimationOptions.CurveEaseInOut,
                animations:
                {
                    self.lineFour.alpha = 1.0
                },
                completion: nil)
        }
        
        /* Line Five */
        
        // Hide Fifth For iPhone 5 & Below //
        if !DeviceType.IS_IPHONE_5 && !DeviceType.IS_IPHONE_4_OR_LESS {
            UIView.animateWithDuration(0.8, delay: 0.6, options: .CurveEaseInOut,
                animations:
                {
                    self.lineFive.alpha = 1.0
                },
                completion: nil)
        }
    }
    
    // MARK: Days & Humidity
    func animateLeftSide() {
        /* Days */
        todayLabel.alpha = 0.0
        tomorrowLabel.alpha = 0.0
        dayWeekLabel.alpha = 0.0
        dayWeekTwoLabel.alpha = 0.0
        /* Humidity Headers */
        humidityOne.alpha = 0.0
        humidityTwo.alpha = 0.0
        humidityThree.alpha = 0.0
        humidityFour.alpha = 0.0
        /* Humidity Values */
        hValueOne.alpha = 0.0
        hValueTwo.alpha = 0.0
        hValueThree.alpha = 0.0
        hvalueFour.alpha = 0.0
        
        /* First */
        UIView.animateWithDuration(0.8, delay: 0.3, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.todayLabel.alpha = 1.0
                self.humidityOne.alpha = 1.0
                self.hValueOne.alpha = 1.0
            },
            completion: nil)
        UIView.animateWithDuration(0.8, delay: 0.3, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.todayLabel.center.x = self.todayLabel.center.x + 30
                self.humidityOne.center.x = self.humidityOne.center.x + 30
                self.hValueOne.center.x = self.hValueOne.center.x + 30
            },
            completion: nil)
        
        /* Second */
        UIView.animateWithDuration(0.8, delay: 0.4, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.tomorrowLabel.alpha = 1.0
                self.humidityTwo.alpha = 1.0
                self.hValueTwo.alpha = 1.0
            },
            completion: nil)
        UIView.animateWithDuration(0.8, delay: 0.4, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.tomorrowLabel.center.x = self.tomorrowLabel.center.x + 30
                self.humidityTwo.center.x = self.humidityTwo.center.x + 30
                self.hValueTwo.center.x = self.hValueTwo.center.x + 30
            },
            completion: nil)
        
        /* Third */
        
        // Hide Third For iPhone 4 //
        if !DeviceType.IS_IPHONE_4_OR_LESS {
            UIView.animateWithDuration(0.8, delay: 0.5, options: UIViewAnimationOptions.CurveEaseInOut,
                animations:
                {
                    self.dayWeekLabel.alpha = 1.0
                    self.humidityThree.alpha = 1.0
                    self.hValueThree.alpha = 1.0
                },
                completion: nil)
            UIView.animateWithDuration(0.8, delay: 0.5, options: UIViewAnimationOptions.CurveEaseInOut,
                animations:
                {
                    self.dayWeekLabel.center.x = self.dayWeekLabel.center.x + 30
                    self.humidityThree.center.x = self.humidityThree.center.x + 30
                    self.hValueThree.center.x = self.hValueThree.center.x + 30
                },
                completion: nil)
        }
        
        /* Fourth */
        
        // Hide Fourth For iPhone 5 & Below //
        if !DeviceType.IS_IPHONE_5 && !DeviceType.IS_IPHONE_4_OR_LESS {
            UIView.animateWithDuration(0.8, delay: 0.6, options: UIViewAnimationOptions.CurveEaseInOut,
                animations:
                {
                    self.dayWeekTwoLabel.alpha = 1.0
                    self.humidityFour.alpha = 1.0
                    self.hvalueFour.alpha = 1.0
                },
                completion: nil)
            UIView.animateWithDuration(0.8, delay: 0.5, options: UIViewAnimationOptions.CurveEaseInOut,
                animations:
                {
                    self.dayWeekTwoLabel.center.x = self.dayWeekTwoLabel.center.x + 30
                    self.humidityFour.center.x = self.humidityFour.center.x + 30
                    self.hvalueFour.center.x = self.hvalueFour.center.x + 30
                },
                completion: nil)
        }
    }
    
    // MARK: Rain
    func animateRightSide() {
        /* Percent Value */
        rValueOne.alpha = 0.0
        rValueTwo.alpha = 0.0
        rValueThree.alpha = 0.0
        rvalueFour.alpha = 0.0
        /* Chance Headers */
        chanceOne.alpha = 0.0
        chanceTwo.alpha = 0.0
        chanceThree.alpha = 0.0
        chanceFour.alpha = 0.0
        
        /* First */
        UIView.animateWithDuration(0.8, delay: 0.3, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.rValueOne.alpha = 1.0
                self.chanceOne.alpha = 1.0
            },
            completion: nil)
        UIView.animateWithDuration(0.8, delay: 0.3, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.rValueOne.center.x = self.rValueOne.center.x - 30
                self.chanceOne.center.x = self.chanceOne.center.x - 30
            },
            completion: nil)
        
        /* Second */
        UIView.animateWithDuration(0.8, delay: 0.4, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.rValueTwo.alpha = 1.0
                self.chanceTwo.alpha = 1.0
            },
            completion: nil)
        UIView.animateWithDuration(0.8, delay: 0.4, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.rValueTwo.center.x = self.rValueTwo.center.x - 30
                self.chanceTwo.center.x = self.chanceTwo.center.x - 30
            },
            completion: nil)
        
        /* Third */
        
        // Hide Third For iPhone 4 //
        if !DeviceType.IS_IPHONE_4_OR_LESS {
            UIView.animateWithDuration(0.8, delay: 0.5, options: UIViewAnimationOptions.CurveEaseInOut,
                animations:
                {
                    self.rValueThree.alpha = 1.0
                    self.chanceThree.alpha = 1.0
                },
                completion: nil)
            UIView.animateWithDuration(0.8, delay: 0.5, options: UIViewAnimationOptions.CurveEaseInOut,
                animations:
                {
                    self.rValueThree.center.x = self.rValueThree.center.x - 30
                    self.chanceThree.center.x = self.chanceThree.center.x - 30
                },
                completion: nil)
        }
        
        /* Fifth */
        
        // Hide Fifth For iPhone 5 & Below //
        if !DeviceType.IS_IPHONE_5 && !DeviceType.IS_IPHONE_4_OR_LESS {
            UIView.animateWithDuration(0.8, delay: 0.6, options: UIViewAnimationOptions.CurveEaseInOut,
                animations:
                {
                    self.rvalueFour.alpha = 1.0
                    self.chanceFour.alpha = 1.0
                },
                completion: nil)
            UIView.animateWithDuration(0.8, delay: 0.6, options: UIViewAnimationOptions.CurveEaseInOut,
                animations:
                {
                    self.rvalueFour.center.x = self.rvalueFour.center.x - 30
                    self.chanceFour.center.x = self.chanceFour.center.x - 30
                },
                completion: nil)
        }
    }
}