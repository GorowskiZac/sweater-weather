//
//  CircleTransitionAnimator.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 11/10/15.
//  Copyright © 2015 Tetragon. All rights reserved.
//

import UIKit

class CircleTransitionAnimator: NSObject,UIViewControllerAnimatedTransitioning {
    
    weak var transitionContext: UIViewControllerContextTransitioning?
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.5
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        if mainScreenCheck! == 0 {
            
            self.transitionContext = transitionContext
            
            let containerView = transitionContext.containerView()
            let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as! ViewController
            let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as! DetailViewController
            let button = fromViewController.arrowBtnOutlet
            containerView!.addSubview(toViewController.view)
            
            let circleMaskPathInitial = UIBezierPath(ovalInRect: button.frame)
            let extremePoint = CGPoint(x: button.center.x - 0, y: button.center.y + CGRectGetHeight(toViewController.view.bounds))
            let radius = sqrt((extremePoint.x*extremePoint.x) + (extremePoint.y*extremePoint.y))
            let circleMaskPathFinal = UIBezierPath(ovalInRect: CGRectInset(button.frame, -radius, -radius))
            
            let maskLayer = CAShapeLayer()
            maskLayer.path = circleMaskPathFinal.CGPath
            toViewController.view.layer.mask = maskLayer
            
            let maskLayerAnimation = CABasicAnimation(keyPath: "path")
            maskLayerAnimation.fromValue = circleMaskPathInitial.CGPath
            maskLayerAnimation.toValue = circleMaskPathFinal.CGPath
            maskLayerAnimation.duration = self.transitionDuration(transitionContext)
            maskLayerAnimation.delegate = self
            maskLayer.addAnimation(maskLayerAnimation, forKey: "path")
        } else if mainScreenCheck == 1 {
            
            self.transitionContext = transitionContext
            
            let containerView = transitionContext.containerView()
            let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as! DetailViewController
            let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as! ViewController
            let button = fromViewController.arrowBtnOutlet
            containerView!.addSubview(toViewController.view)
            
            let circleMaskPathInitial = UIBezierPath(ovalInRect: button.frame)
            let extremePoint = CGPoint(x: button.center.x - 0, y: button.center.y + CGRectGetHeight(toViewController.view.bounds))
            let radius = sqrt((extremePoint.x*extremePoint.x) + (extremePoint.y*extremePoint.y))
            let circleMaskPathFinal = UIBezierPath(ovalInRect: CGRectInset(button.frame, -radius, -radius))
            
            let maskLayer = CAShapeLayer()
            maskLayer.path = circleMaskPathFinal.CGPath
            toViewController.view.layer.mask = maskLayer
            
            let maskLayerAnimation = CABasicAnimation(keyPath: "path")
            maskLayerAnimation.fromValue = circleMaskPathInitial.CGPath
            maskLayerAnimation.toValue = circleMaskPathFinal.CGPath
            maskLayerAnimation.duration = self.transitionDuration(transitionContext)
            maskLayerAnimation.delegate = self
            maskLayer.addAnimation(maskLayerAnimation, forKey: "path")
        }
    }
    
    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        self.transitionContext?.completeTransition(!self.transitionContext!.transitionWasCancelled())
        self.transitionContext?.viewControllerForKey(UITransitionContextFromViewControllerKey)?.view.layer.mask = nil
    }

}
