//
//  LocationHelper.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 10/31/15.
//  Copyright © 2015 Tetragon. All rights reserved.
//

import Foundation
import MapKit

extension ViewController {
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Location ERROR:  \(error)")
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.AuthorizedAlways {
            // Set Location Accurace //
            locationMngr.desiredAccuracy = kCLLocationAccuracyHundredMeters
            // Begin Updating Location //
            locationMngr.startUpdatingLocation()
            
            /* Dismiss Fail Alert */
            if failAlert != nil {
                failAlert!.dismissViewControllerAnimated(true, completion: nil)
            }
        } else {
            // Notify User of Denied Permission //
            showLocationDeniedAlert()
            activityOutlet.stopAnimating()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLat = manager.location!.coordinate.latitude
        let userLng = manager.location!.coordinate.longitude
        userLatLngArray = [userLat as Double, userLng as Double]
        
        if callGuard < 1 {
            
            /* Set URL */
            let url = NSURL(string: "https://api.forecast.io/forecast/e308e7efb0cebaad4be3b473acc2e380/\(Double(userLat)),\(Double(userLng))")
            
            /* Call API */
            let request = NSMutableURLRequest(URL: url!)
            httpGet(request)
        }
        callGuard++
        locationMngr.stopUpdatingLocation()
    }
    
}
