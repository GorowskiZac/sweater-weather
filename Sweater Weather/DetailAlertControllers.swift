//
//  DetailAlertControllers.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 3/7/16.
//  Copyright © 2016 Tetragon. All rights reserved.
//

import Foundation
import UIKit

extension DetailViewController {
    /* Memory Warning Alert */
    func showMemoryAlert() {
        /* Low Memory; Notify User */
        let memoryAlert = UIAlertController(title: "Low Memory", message: "It looks like your device is running low on memory. This may cuase slow load times as well as odd behavior within this application.", preferredStyle: .Alert)
        let okayAlert = UIAlertAction(title: "Okay", style: .Cancel, handler: nil)
        memoryAlert.addAction(okayAlert)
        self.presentViewController(memoryAlert, animated: true, completion: nil)
    }
}