//
//  AnimationHelper.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 11/10/15.
//  Copyright © 2015 Tetragon. All rights reserved.
//

import Foundation
import UIKit

extension ViewController {
    
    // MARK: Main Message
    func animateMainMessage() {
        /* Stop Activity Indicator */
        if activityOutlet.isAnimating() && activityOutlet.hidden == false {
            activityOutlet.hidden = true
            activityOutlet.stopAnimating()
        }
        
        mainMessageLabel.alpha = 0.0
        mainMessageLabel.center.y += 55
        
        UIView.animateWithDuration(0.8, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.mainMessageLabel.alpha = 1.0;
                self.mainMessageLabel.center.y = self.mainMessageLabel.center.y - 55;
            },
            completion: nil)
    }
    
    // MARK: Main Status
    func animateStatus() {
        statusMessageLabel.alpha = 0.0
        statusMessageLabel.center.y += 20
        
        UIView.animateWithDuration(0.7, delay: 0.4, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.statusMessageLabel.alpha = 1.0;
                self.statusMessageLabel.center.y = self.statusMessageLabel.center.y - 20;
            },
            completion: nil)
    }
    
    // MARK: Main Temp Items
    func animateTempItems() {
        mainTempOne.alpha = 0.0
        mainTempTwo.alpha = 0.0
        mainDegreeLabel.alpha = 0.0
        mainUnitLabel.alpha = 0.0
        
        mainTempOne.center.y += 55
        mainTempTwo.center.y += 55
        mainDegreeLabel.center.y += 75
        mainUnitLabel.center.y += 30
        
        //////* First Temp Number *//////
        UIView.animateWithDuration(0.5, delay: 0.6, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.mainTempOne.alpha = 1.0;
                self.mainTempOne.center.y = self.mainTempOne.center.y - 55;
            },
            completion: nil)
        
        //////* Second Temp Number *//////
        UIView.animateWithDuration(0.65, delay: 0.8, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.mainTempTwo.alpha = 1.0;
                self.mainTempTwo.center.y = self.mainTempTwo.center.y - 55;
            },
            completion: nil)
        
        //////* Degree Symbol *//////
        UIView.animateWithDuration(0.9, delay: 1.0, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.mainDegreeLabel.alpha = 1.0;
                self.mainDegreeLabel.center.y = self.mainDegreeLabel.center.y - 75;
            },
            completion: nil)
        
        //////* Unit Symbol *//////
        UIView.animateWithDuration(0.7, delay: 1.2, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.mainUnitLabel.alpha = 1.0;
                self.mainUnitLabel.center.y = self.mainUnitLabel.center.y - 30;
            },
            completion: nil)
    }
    
    // MARK: Summary Message
    func animateSummary() {
        summaryMessageLabel.alpha = 0.0
        summaryMessageLabel.center.y += 30
        
        UIView.animateWithDuration(1.0, delay: 1.2, options: .CurveEaseInOut,
            animations:
            {
                self.summaryMessageLabel.alpha = 1.0;
                self.summaryMessageLabel.center.y = self.summaryMessageLabel.center.y - 30;
            },
            completion: nil)
    }
    
    // MARK: Arrow Image
    func animateArrow() {
        arrowBtnOutlet.alpha = 0.0
        arrowBtnOutlet.center.y += 20
        
        UIView.animateWithDuration(1.2, delay: 1.5, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.arrowBtnOutlet.alpha = 1.0
            },
            completion: nil)
        UIView.animateWithDuration(1.2, delay: 1.5, options: UIViewAnimationOptions.CurveEaseInOut,
            animations:
            {
                self.arrowBtnOutlet.center.y = self.arrowBtnOutlet.center.y - 20
            },
            completion: nil)
    }
}
