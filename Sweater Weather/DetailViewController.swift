//
//  DetailViewController.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 11/10/15.
//  Copyright © 2015 Tetragon. All rights reserved.
//

import UIKit
import SweaterKit

class DetailViewController: UIViewController {
    
    // MARK: Outlets
    /* Lines */
    @IBOutlet weak var lineOne: UIView!
    @IBOutlet weak var lineTwo: UIView!
    @IBOutlet weak var lineThree: UIView!
    @IBOutlet weak var lineFour: UIView!
    @IBOutlet weak var lineFive: UIView!
    
    /* Top Temps */
    @IBOutlet weak var topTempHigh: UILabel!
    @IBOutlet weak var topTempLow: UILabel!
    @IBOutlet weak var topHighLabel: UILabel!
    @IBOutlet weak var topLowLabel: UILabel!
    
    /* Days */
    @IBOutlet weak var todayLabel: UILabel!
    @IBOutlet weak var tomorrowLabel: UILabel!
    @IBOutlet weak var dayWeekLabel: UILabel!
    @IBOutlet weak var dayWeekTwoLabel: UILabel!
    
    /* Humidity Headers */
    @IBOutlet weak var humidityOne: UILabel!
    @IBOutlet weak var humidityTwo: UILabel!
    @IBOutlet weak var humidityThree: UILabel!
    @IBOutlet weak var humidityFour: UILabel!
    
    /* Humidity Values */
    @IBOutlet weak var hValueOne: UILabel!
    @IBOutlet weak var hValueTwo: UILabel!
    @IBOutlet weak var hValueThree: UILabel!
    @IBOutlet weak var hvalueFour: UILabel!
    
    /* Rain Headers */
    @IBOutlet weak var chanceOne: UILabel!
    @IBOutlet weak var chanceTwo: UILabel!
    @IBOutlet weak var chanceThree: UILabel!
    @IBOutlet weak var chanceFour: UILabel!
    
    /* Rain Values */
    @IBOutlet weak var rValueOne: UILabel!
    @IBOutlet weak var rValueTwo: UILabel!
    @IBOutlet weak var rValueThree: UILabel!
    @IBOutlet weak var rvalueFour: UILabel!
    
    // MARK: Passed Data
    var weatherObject : WeatherObject!
    var unitSetting : Bool!
    
    // MARK: Transition Button
    /* Arrow Outlet */
    @IBOutlet weak var arrowBtnOutlet: UIButton!
    /* Arrow Action */
    @IBAction func arrowBtnAction(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    // MARK: View Setup
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "DETAIL"
        
        // Set Arrow Button Alpha //
        arrowBtnOutlet.alpha = 1.0

        // Hide Navigation Bar //
        navigationController?.navigationBarHidden = true
        
        /* Label Setup */
        // Top //
        
        if unitSetting != nil {
            if unitSetting == true {
                topTempHigh.text = "\(weatherObject.todayHighF)º"
                topTempLow.text = "\(weatherObject.todayLowF)º"
            } else {
                topTempHigh.text = "\(weatherObject.todayHighC)º"
                topTempLow.text = "\(weatherObject.todayLowC)º"
            }
        }
        // Today //
        hValueOne.text = "\(weatherObject.todayHumid)%"
        rValueOne.text = "\(weatherObject.todayRain)%"
        // Tomorrow //
        hValueTwo.text = "\(weatherObject.dayTwoHumid)%"
        rValueTwo.text = "\(weatherObject.dayTwoRain)%"
        // Third Day //
        hValueThree.text = "\(weatherObject.dayThreeHumid)%"
        rValueThree.text = "\(weatherObject.dayThreeRain)%"
        dayWeekLabel.text = setFutureDay(1)
        // Fourth Day //
        hvalueFour.text = "\(weatherObject.dayFourHumid)%"
        rvalueFour.text = "\(weatherObject.dayFourRain)%"
        dayWeekTwoLabel.text = setFutureDay(2)
        
        /* Animations */
        animateTopTemps()
        animateLines()
        animateLeftSide()
        animateRightSide()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        setRainSnowChance()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

    // MARK: Memory Warning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        showMemoryAlert()
    }
    
    // MARK: Day Parsing
    func setFutureDay(daysAhead: Int) -> String {
        let calendar : NSCalendar = NSCalendar.currentCalendar()
        let dateComps : NSDateComponents = calendar.components(.Weekday , fromDate: NSDate())
        
        var dayOfWeek = (dateComps.weekday + 7 - calendar.firstWeekday) % 7 + daysAhead
        
        if dayOfWeek == 8 {
            dayOfWeek = 1
        }else if dayOfWeek == 9 {
            dayOfWeek = 2
        } else if dayOfWeek == 10 {
            
        }
        
        switch dayOfWeek {
        case 1:
            return "Tuesday"
        case 2:
            return "Wednesday"
        case 3:
            return "Thursday"
        case 4:
            return "Friday"
        case 5:
            return "Saturday"
        case 6:
            return "Sunday"
        case 7:
            return "Monday"
        default:
            print("FUTURE-DAY-OF-WEEK:  **ERROR**  Controllers -> DetailViewController -> setFutureDay()")
            return ""
        }
    }

}
