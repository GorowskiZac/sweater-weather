//
//  SetupMainViews.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 3/3/16.
//  Copyright © 2016 Tetragon. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit
import SweaterKit

extension ViewController {
    func setupMainViews() {
        /* Condition Arrays For Background Image */
        let sunArray = ["clear-day", "wind", "partly-cloudy-day", "cloudy-day", "cloudy", "cloudy-day"]
        let rainArray = ["rain", "thunderstorm", "tornado", "fog"]
        let snowArray = ["snow", "sleet", "hail"]
        let nightArray = ["cloudy-night", "partly-cloudy-night", "clear-night"]
        
        
        /* Check Beta Settings & BG String */
        if self.betaSelected && self.weatherObject.currentIcon != "" {
            
            UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseInOut, animations: {self.backgroundImageOutlet.alpha = 0.0}, completion: nil)
            
            if sunArray.contains(self.weatherObject.currentIcon) {
                self.backgroundImageOutlet.image = UIImage(named: "sunnyCitySix")
                UIView.animateWithDuration(0.4, delay: 0.0, options: .CurveEaseInOut, animations:
                    {
                        self.backgroundImageOutlet.hidden = false;
                        self.backgroundImageOutlet.alpha = 0.80
                    },
                    completion: nil)
            } else if rainArray.contains(self.weatherObject.currentIcon) {
                /* Build SpriteKit Scene */
                setupSpriteScene()
                
                /* Set Sprite */
                let rainPath = NSBundle.mainBundle().pathForResource("RainParticle", ofType: "sks")
                let rainParticle = NSKeyedUnarchiver.unarchiveObjectWithFile(rainPath!) as! SKEmitterNode
                rainParticle.position = CGPointMake(self.view.bounds.width / 2, self.view.bounds.height)
                self.skWeatherScene?.addChild(rainParticle)
                
                /* Set BG Image */
                self.backgroundImageOutlet.image = UIImage(named: "rainCitySix")
                UIView.animateWithDuration(0.4, delay: 0.0, options: .CurveEaseInOut, animations:
                    {
                        self.backgroundImageOutlet.hidden = false;
                        self.backgroundImageOutlet.alpha = 0.75
                    },
                    completion: nil)
            } else if snowArray.contains(self.weatherObject.currentIcon) {
                /* Build SpriteKit Scene */
                setupSpriteScene()
                
                /* Set Sprite */
                let snowPath = NSBundle.mainBundle().pathForResource("SnowParticle", ofType: "sks")
                let snowParticle = NSKeyedUnarchiver.unarchiveObjectWithFile(snowPath!) as! SKEmitterNode
                snowParticle.position = CGPointMake(self.view.bounds.width / 2, self.view.bounds.height)
                self.skWeatherScene?.addChild(snowParticle)
                
                /* Set BG Image */
                self.backgroundImageOutlet.image = UIImage(named: "snowTrainSix")
                UIView.animateWithDuration(0.4, delay: 0.0, options: .CurveEaseInOut, animations:
                    {
                        self.backgroundImageOutlet.hidden = false;
                        self.backgroundImageOutlet.alpha = 0.7
                    },
                    completion: nil)
            } else if nightArray.contains(self.weatherObject.currentIcon) {
                self.backgroundImageOutlet.image = UIImage(named: "nightCitySix")
                UIView.animateWithDuration(0.4, delay: 0.0, options: .CurveEaseInOut, animations:
                    {
                        self.backgroundImageOutlet.hidden = false;
                        self.backgroundImageOutlet.alpha = 0.8
                    },
                    completion: nil)
            } else {
                self.backgroundImageOutlet.hidden = true
            }
        } else {
            self.backgroundImageOutlet.hidden = true
        }
        
        if self.checkUnit() {
            // Fahrenheit //
            self.mainUnitLabel.text = "F"
            let firstNum = "\(self.weatherObject.currentTempF)"["\(self.weatherObject.currentTempF)".startIndex.advancedBy(0)]
            let secondNum = "\(self.weatherObject.currentTempF)"["\(self.weatherObject.currentTempF)".startIndex.advancedBy(1)]
            self.mainTempOne.text = "\(firstNum)"
            self.mainTempTwo.text = "\(secondNum)"
            self.mainDegreeLabel.text = "º"
            
            self.mainMessageLabel.text = ParseTemp(temp: Double(self.weatherObject.currentTempF)).messageText
            self.statusMessageLabel.text = ParseTemp(temp: Double(self.weatherObject.currentTempF)).conditionText
        } else {
            // Celsius //
            self.mainUnitLabel.text = "C"
            let firstNum = "\(self.weatherObject.currentTempC)"["\(self.weatherObject.currentTempC)".startIndex.advancedBy(0)]
            let secondNum = "\(self.weatherObject.currentTempC)"["\(self.weatherObject.currentTempC)".startIndex.advancedBy(1)]
            self.mainTempOne.text = "\(firstNum)"
            self.mainTempTwo.text = "\(secondNum)"
            self.mainDegreeLabel.text = "º"
            
            self.mainMessageLabel.text = ParseTemp(temp: Double(self.weatherObject.currentTempC)).messageText
            self.statusMessageLabel.text = ParseTemp(temp: Double(self.weatherObject.currentTempC)).conditionText
        }
        
        /* Check Summary Text */
        if self.weatherObject.todaySummary != "" {
            self.summaryMessageLabel.text = self.weatherObject.todaySummary
        }
        
        /* Animate UI */
        self.animateMainMessage()
        self.animateStatus()
        self.animateTempItems()
        self.animateSummary()
        self.animateArrow()
    }
    
    // MARK: Hide Views
    func hideViews() {
        UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseInOut, animations:
            {
                self.backgroundImageOutlet.alpha = 0.0
                self.mainMessageLabel.alpha = 0.0
                self.statusMessageLabel.alpha = 0.0
                self.mainTempOne.alpha = 0.0
                self.mainTempTwo.alpha = 0.0
                self.mainUnitLabel.alpha = 0.0
                self.mainDegreeLabel.alpha = 0.0
                self.summaryMessageLabel.alpha = 0.0
                self.arrowBtnOutlet.alpha = 0.0
            }
            , completion:
            {finish in
                /* Reload API Data */
                self.loadData(true)
            }
        )
    }
}
