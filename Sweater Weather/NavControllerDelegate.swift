//
//  NavControllerDelegate.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 11/10/15.
//  Copyright © 2015 Tetragon. All rights reserved.
//

import UIKit

class NavControllerDelegate: NSObject, UINavigationControllerDelegate {
    
    func navigationController(navigationController: UINavigationController,
        animationControllerForOperation operation: UINavigationControllerOperation,
        fromViewController fromVC: UIViewController,
        toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
            if fromVC.title == "MAIN" || fromVC.title == "DETAIL" {
                return CircleTransitionAnimator()
            } else {
                return nil
            }
    }
    
    var interactionController: UIPercentDrivenInteractiveTransition?
    
    func navigationController(navigationController: UINavigationController,
        interactionControllerForAnimationController animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
            return self.interactionController
    }

}
