//
//  ViewController.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 10/31/15.
//  Copyright © 2015 Tetragon. All rights reserved.
//

import UIKit
import MapKit
import SpriteKit
import SweaterKit

var mainScreenCheck : Int?

class ViewController: UIViewController, CLLocationManagerDelegate, UIViewControllerTransitioningDelegate {
    
    // MARK: Main Status Outlets
    @IBOutlet weak var mainMessageLabel: UILabel!
    @IBOutlet weak var statusMessageLabel: UILabel!
    @IBOutlet weak var mainTempOne: UILabel!
    @IBOutlet weak var mainTempTwo: UILabel!
    @IBOutlet weak var mainUnitLabel: UILabel!
    @IBOutlet weak var mainDegreeLabel: UILabel!
    @IBOutlet weak var summaryMessageLabel: UILabel!
    
    // MARK: Activity Indicator
    @IBOutlet weak var activityOutlet: UIActivityIndicatorView!
    
    // MARK: Background Image
    @IBOutlet weak var backgroundImageOutlet: UIImageView!
    
    // MAKR: Settings Preferences
    var betaSelected = false
    var unitSelected = true
    
    // MARK: API Management
    var myData = NSMutableData()
    var callGuard = 0
    
    // MARK: Location Items
    var userLatLngArray : [Double] = []
    let locationMngr = CLLocationManager()
    var failAlert : UIAlertController?
    
    // MARK: Weather Items
    var weatherObject : WeatherObject!
    
    // MARK: Sprite Items
    var skWeatherView : SKView?
    var skWeatherScene : SKScene?
    
    // MARK: Transition Button
    /* Arrow Outlet */
    @IBOutlet weak var arrowBtnOutlet: UIButton!
    /* Arrow Action */
    @IBAction func arrowBtnAction(sender: UIButton) {
        /* Hide All Views */
        hideViews()
        /* Check Beta Preference */
        checkBeta()
    }

    // MARK: View Setup
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "MAIN"
        
        /* Check Beta Preference */
        checkBeta()
        
        /* Check Network */
        if Reachability().isConnected == true {
            /* Get Location & API Data */
            loadData(false)
        } else {
            /* No Connection; Notify User */
            showNetworkAlert()
        }
        
        /* Hide Navigation Bar */
        navigationController?.navigationBarHidden = true
        
        callGuard = 0
        arrowBtnOutlet.alpha = 0.0
    }
    
    override func viewWillAppear(animated: Bool) {
        
        /* Make Sure We Animate Once Per Load */
        if mainMessageLabel.text?.characters.count > 0 {
            /* Animate Views */
            animateMainMessage()
            animateStatus()
            animateTempItems()
            animateSummary()
            animateArrow()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        mainScreenCheck = 0
    }
    
    override func viewDidDisappear(animated: Bool) {
        mainScreenCheck = 1
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PushSegue" {
            
            let destVC = segue.destinationViewController as! DetailViewController
            
            destVC.weatherObject = weatherObject
            destVC.unitSetting = checkUnit()
        }
    }
    
    // MARK: Memory Warning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        showMemoryAlert()
    }
    
    // MARK: Settings
    func checkBeta() {
        /* Get Beta Value From Settings */
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if let betaValue = userDefaults.valueForKey("beta_preference")?.boolValue {
            betaSelected = betaValue
            if betaValue {
                backgroundImageOutlet.hidden = false
            } else {
                backgroundImageOutlet.hidden = true
            }
        } else {
            betaSelected = false
            backgroundImageOutlet.hidden = true
        }
    }
    
    func checkUnit() -> Bool {
        /* Get Unit Value From Settings */
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if let unitValue = userDefaults.valueForKey("unit_preference")?.intValue {
            if unitValue == 0 {
                return false
            } else {
                return true
            }
        } else {
            print("No Settings Value Found")
            return true
        }
    }
}