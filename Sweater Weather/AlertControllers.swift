//
//  AlertControllers.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 3/3/16.
//  Copyright © 2016 Tetragon. All rights reserved.
//

import Foundation
import UIKit

extension ViewController {
    
    /* Network Alert */
    func showNetworkAlert() {
        let networkAlert = UIAlertController(title: "Network Error", message: "It looks like we're having some problems accessing your network. Please make sure network is connected and stable.", preferredStyle: .Alert)
        let okayAlert = UIAlertAction(title: "Okay", style: .Cancel, handler: nil)
        networkAlert.addAction(okayAlert)
        self.presentViewController(networkAlert, animated: true, completion: nil)
    }
    
    /* Location Access Alert */
    func showLocationDeniedAlert() {
        // Build Failed Permission Alert //
        failAlert = UIAlertController(title: "You've Denied \"Sweater Weather \" Access To Your Location", message: "You can toggle locaiton service permissions in Settings.", preferredStyle: .Alert)
        let okayAction = UIAlertAction(title: "Okay", style: .Cancel, handler: nil)
        failAlert?.addAction(okayAction)
        // Present Alert //
        presentViewController(failAlert!, animated: true, completion: nil)
    }
    
    /* Connection Error Alert */
    func showConnectionAlert() {
        /* Network Alert */
        let alertController = UIAlertController(title: "Network Error", message: "It looks like we're having some problems accessing your network. Please make sure network is connected and stable.", preferredStyle: .Alert)
        
        /* Alert Actions */
        let doneAction = UIAlertAction(title: "Okay", style: .Cancel, handler: nil)
        
        /* Adding Acitons & Presenting */
        alertController.addAction(doneAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    /* Memory Warning Alert */
    func showMemoryAlert() {
        /* Low Memory; Notify User */
        let memoryAlert = UIAlertController(title: "Low Memory", message: "It looks like your device is running low on memory. This may cuase slow load times as well as odd behavior within this application.", preferredStyle: .Alert)
        let okayAlert = UIAlertAction(title: "Okay", style: .Cancel, handler: nil)
        memoryAlert.addAction(okayAlert)
        self.presentViewController(memoryAlert, animated: true, completion: nil)
    }
}