//
//  ApiHelper.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 10/31/15.
//  Copyright © 2015 Tetragon. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit
import SweaterKit
import SweaterKit

extension ViewController {
    
    // MARK: Start Loading Data
    func loadData(refreshing: Bool) {
        /* Start Activity Indicator */
        activityOutlet.startAnimating()
        activityOutlet.hidden = false
        
        /* Check For Network */
        if Reachability().isConnected == true {
            
            if refreshing {
                callGuard = 0
                if callGuard < 1 {
                    
                    /* Set URL */
                    let url = NSURL(string: "https://api.forecast.io/forecast/e308e7efb0cebaad4be3b473acc2e380/\(Double(userLatLngArray[0])),\(Double(userLatLngArray[1]))")
                    
                    /* Call API */
                    let request = NSMutableURLRequest(URL: url!)
                    httpGet(request)
                    callGuard++
                }
            } else {
                // Setup Location Manager
                locationMngr.delegate = self
                locationMngr.requestAlwaysAuthorization()
            }
        } else {
            print("ERROR :: No Internet Connection")
            
            /* Stop Activity Indicator */
            activityOutlet.hidden = true
            activityOutlet.stopAnimating()
            
            showConnectionAlert()
        }
    }
    
    func URLSession(session: NSURLSession, didReceiveChallenge challenge: NSURLAuthenticationChallenge, completionHandler: (NSURLSessionAuthChallengeDisposition, NSURLCredential?) -> Void) {
        print("NSSession: Did Receive Challenged -> \(challenge)")
    }
    
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveData data: NSData) {
        print("data")
    }
    
    // MARK: Get Request
    func httpGet(request: NSURLRequest!) -> Void {

        /* Set Up & Execute Session */
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request){ (data, response, error) -> Void in
            if error != nil {
                print(error)
            } else {
                let json = data
                self.myData = NSMutableData(data: json!);
                
                /* Dispatch Asyncronously */
                let queue = dispatch_queue_create("com.tetragon.zgski.Sweater-Weather", DISPATCH_QUEUE_CONCURRENT)
                dispatch_sync(queue, {
                    
                    if self.myData.length != 0 {
                        let response = JSON(data: self.myData)
                        /* Gets Icon For Background Image */
                        let currentIcon = "\(response["currently"]["icon"])"
                        
                        /* Gets Current Temperature */
                        let temp = Double("\(response["currently"]["temperature"])")!
                        /* Temp In Fahrenheit */
                        let currentTemp = Int(round(temp))
                        
                        /* Gets Daily Data */
                        if let days = response["daily"]["data"].array {
                            
                            // Fahrenheit //
                            let todayHigh = Int(round(Double("\(days[0]["temperatureMax"])")!))
                            let todayLow = Int(round(Double("\(days[0]["temperatureMin"])")!))
                            
                            /* Today */
                            let todayHumid = Int((Float("\(days[0]["humidity"])")! * 100.0))
                            let todayRain = Int((Float("\(days[0]["precipProbability"])")! * 100.0))
                            let todaySummary = "\(days[0]["summary"])"
                            
                            /* Tomorrow */
                            let dayTwoHigh = Int(round(Double("\(days[1]["temperatureMax"])")!))
                            let dayTwoLow = Int(round(Double("\(days[1]["temperatureMin"])")!))
                            let dayTwoHumid = Int((Float("\(days[1]["humidity"])")! * 100.0))
                            let dayTwoRain = Int((Float("\(days[1]["precipProbability"])")! * 100.0))
                            
                            /* Third Day */
                            let dayThreeHigh = Int(round(Double("\(days[2]["temperatureMax"])")!))
                            let dayThreeLow = Int(round(Double("\(days[2]["temperatureMin"])")!))
                            let dayThreeHumid = Int((Float("\(days[2]["humidity"])")! * 100.0))
                            let dayThreeRain = Int((Float("\(days[2]["precipProbability"])")! * 100.0))
                            
                            /* Fourth Day */
                            let dayFourHigh = Int(round(Double("\(days[3]["temperatureMax"])")!))
                            let dayFourLow = Int(round(Double("\(days[3]["temperatureMin"])")!))
                            let dayFourHumid = Int((Float("\(days[3]["humidity"])")! * 100.0))
                            let dayFourRain = Int((Float("\(days[3]["precipProbability"])")! * 100.0))
                            
                            /* Set Object Properties */
                            self.weatherObject = WeatherObject(currentTempF: currentTemp, currentIcon: currentIcon, todayHighF: todayHigh, todayLowF: todayLow, todayRain: todayRain, todayHumid: todayHumid, todaySummary: todaySummary, dayTwoHigh: dayTwoHigh, dayTwoLow: dayTwoLow, dayTwoRain: dayTwoRain, dayTwoHumid: dayTwoHumid, dayThreeHigh: dayThreeHigh, dayThreeLow: dayThreeLow, dayThreeRain: dayThreeRain, dayThreeHumid: dayThreeHumid, dayFourHigh: dayFourHigh, dayFourLow: dayFourLow, dayFourRain: dayFourRain, dayFourHumid: dayFourHumid)
                        }
                    }
                    
                    // MARK: UI Setup
                    dispatch_async(dispatch_get_main_queue(), {
                        self.setupMainViews()
                    })
                    
                })
                
            }
        }
        task.resume()
    }
}
