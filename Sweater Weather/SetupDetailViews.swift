//
//  SetupDetailViews.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 3/7/16.
//  Copyright © 2016 Tetragon. All rights reserved.
//

import Foundation
import UIKit

extension DetailViewController {
    
    func setRainSnowChance() {
        let dayAvgTemps = [(weatherObject.dayTwoHigh + weatherObject.dayTwoLow) / 2, (weatherObject.dayThreeHigh + weatherObject.dayThreeLow) / 2, (weatherObject.dayFourHigh + weatherObject.dayFourLow) / 2]
        
        let rainText = "Chance of Rain"
        let snowText = "Chance of Snow"
        
        for avg in dayAvgTemps {
            if avg > 32 {
                // Rain //
                chanceOne.text = rainText
                chanceTwo.text = rainText
                chanceThree.text = rainText
                chanceFour.text = rainText
            } else {
                // Snow //
                chanceOne.text = snowText
                chanceTwo.text = snowText
                chanceThree.text = snowText
                chanceFour.text = snowText
            }
        }
    }
}
