//
//  SceneSetup.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 3/3/16.
//  Copyright © 2016 Tetragon. All rights reserved.
//

import Foundation
import SpriteKit
import UIKit

extension ViewController {
    func setupSpriteScene() {
        /* Set Up SpriteKit */
        let buttonHeight = (arrowBtnOutlet.bounds.size.height + 25) / 2
        
        skWeatherView = SKView(frame: self.view.bounds.offsetBy(dx: 0.0, dy: -buttonHeight).insetBy(dx: 0.0, dy: buttonHeight))
        skWeatherScene = SKScene(size: skWeatherView!.bounds.size)
        
        skWeatherView?.showsDrawCount = false
        skWeatherView?.showsFPS = false
        skWeatherView?.showsNodeCount = false
        
        skWeatherScene?.scaleMode = .AspectFill
        skWeatherScene?.backgroundColor = UIColor.clearColor()
        
        skWeatherView?.allowsTransparency = true
        skWeatherView?.backgroundColor = UIColor.clearColor()
        
        /* Check For Previously Added SKView */
        if skWeatherView?.isDescendantOfView(self.view) == false {
            skWeatherView?.presentScene(skWeatherScene)
            self.view.addSubview(skWeatherView!)
        } else if skWeatherView?.isDescendantOfView(self.view) == true {
            return
        }
    }
}
