//
//  ParseTemp.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 3/3/16.
//  Copyright © 2016 Tetragon. All rights reserved.
//

import Foundation
import UIKit

public class ParseTemp {
    
    public var messageText : String!
    public var conditionText : String!
    
    public init (temp: Double) {
        if temp <= 32.0 {
            self.messageText = "You definitely need a coat"
            self.conditionText = "It's freezing out"
        } else if temp > 32.0 && temp <= 40.0 {
            self.messageText = "You need a coat"
            self.conditionText = "It's pretty cold out"
        } else if temp > 40.0 && temp <= 50.0 {
            self.messageText = "You need a jacket"
            self.conditionText = "It's cold out"
        } else if temp > 50.0 && temp <= 60.0 {
            self.messageText = "You need a jacket"
            self.conditionText = "It's pretty chilly out"
        } else if temp > 60.0 && temp <= 75.0 {
            self.messageText = "You need a sweater"
            self.conditionText = "It's chilly out"
        } else if temp > 75.0 && temp <= 72.0 {
            self.messageText = "You don't need a sweater"
            self.conditionText = "It's pretty nice out"
        } else if temp > 72.0 && temp <= 84.0 {
            self.messageText = "You don't need a sweater"
            self.conditionText = "It's warm out"
        } else if temp > 84.0 && temp <= 91.0 {
            self.messageText = "You don't need a sweater"
            self.conditionText = "It's pretty warm out"
        } else if temp > 91.0 {
            self.messageText = "You definitely don't need a sweater"
            self.conditionText = "It's hot out"
        }
    }
}
