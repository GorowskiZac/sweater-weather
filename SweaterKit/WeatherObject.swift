//
//  WeatherObject.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 3/3/16.
//  Copyright © 2016 Tetragon. All rights reserved.
//

import Foundation

public class WeatherObject: NSObject {
    
    /* Current */
    public var currentTempC : Int!
    public var currentTempF : Int!
    public var currentIcon : String!
    
    /* Today */
    public var todayHighC : Int!
    public var todayHighF : Int!
    public var todayLowC : Int!
    public var todayLowF : Int!
    public var todayRain : Int!
    public var todayHumid : Int!
    public var todaySummary : String!
    
    /* Day 2 */
    public var dayTwoHigh : Int!
    public var dayTwoLow : Int!
    public var dayTwoRain : Int!
    public var dayTwoHumid : Int!
    
    /* Day 3 */
    public var dayThreeHigh : Int!
    public var dayThreeLow : Int!
    public var dayThreeRain : Int!
    public var dayThreeHumid : Int!
    
    /* Day 4 */
    public var dayFourHigh : Int!
    public var dayFourLow : Int!
    public var dayFourRain : Int!
    public var dayFourHumid : Int!
    
    
    /* Initializer */
    public init(currentTempF: Int, currentIcon: String, todayHighF: Int, todayLowF: Int, todayRain: Int, todayHumid: Int, todaySummary: String, dayTwoHigh: Int, dayTwoLow: Int, dayTwoRain: Int, dayTwoHumid: Int, dayThreeHigh: Int, dayThreeLow: Int, dayThreeRain: Int, dayThreeHumid: Int, dayFourHigh: Int, dayFourLow: Int, dayFourRain: Int, dayFourHumid: Int) {
        
        super.init()
        /* Current */
        self.currentTempF = currentTempF
        self.currentTempC = Int( Double(currentTempF - 32) * 0.5556 )
        self.currentIcon = currentIcon
        /* Today */
        self.todayHighF = todayHighF
        self.todayHighC = Int( Double(todayHighF - 32) * 0.5556 )
        self.todayLowF = todayLowF
        self.todayLowC = Int( Double(todayLowF - 32) * 0.5556 )
        self.todayRain = todayRain
        self.todayHumid = todayHumid
        self.todaySummary = todaySummary
        /* Day 2 */
        self.dayTwoHigh = dayTwoHigh
        self.dayTwoLow = dayTwoLow
        self.dayTwoRain = dayTwoRain
        self.dayTwoHumid = dayTwoHumid
        /* Day 3 */
        self.dayThreeHigh = dayThreeHigh
        self.dayThreeLow = dayThreeLow
        self.dayThreeRain = dayThreeRain
        self.dayThreeHumid = dayThreeHumid
        /* Day 4 */
        self.dayFourHigh = dayFourHigh
        self.dayFourLow = dayFourLow
        self.dayFourRain = dayFourRain
        self.dayFourHumid = dayFourHumid
    }
}

