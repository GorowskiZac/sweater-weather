//
//  SweaterKit.h
//  SweaterKit
//
//  Created by Zac Gorowski on 3/3/16.
//  Copyright © 2016 Tetragon. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SweaterKit.
FOUNDATION_EXPORT double SweaterKitVersionNumber;

//! Project version string for SweaterKit.
FOUNDATION_EXPORT const unsigned char SweaterKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SweaterKit/PublicHeader.h>


