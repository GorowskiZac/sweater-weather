//
//  ApiHelper.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 11/1/15.
//  Copyright © 2015 Tetragon. All rights reserved.
//

import Foundation
import UIKit

extension TodayViewController {
    
    func URLSession(session: NSURLSession, didReceiveChallenge challenge: NSURLAuthenticationChallenge, completionHandler: (NSURLSessionAuthChallengeDisposition, NSURLCredential?) -> Void) {
        print("NSSession: Did Receive Challenged -> \(challenge)")
    }
    
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveData data: NSData) {
        print("data")
    }
    
    // MARK: Get Request
    func httpGet(request: NSURLRequest!) -> Void {
        
        var currentTemp : Double = 0.0
        
        /* Set Up & Execute Session */
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request){ (data, response, error) -> Void in
            if error != nil {
                print(error)
            } else {
                let json = data
                self.myData = NSMutableData(data: json!);
                
                /* Dispatch Asyncronously */
                let queue = dispatch_queue_create("com.tetragon.zgski.Sweater-Weather", DISPATCH_QUEUE_CONCURRENT)
                dispatch_sync(queue, {
                    
                    if self.myData.length != 0 {
                        let response = JSON(data: self.myData)
                        
                        if let temp = response["currently"].object["temperature"] {
                            print(temp!)
                            currentTemp = temp as! Double
                        }
                    }
                    
                    // MARK: UI Setup
                    dispatch_async(dispatch_get_main_queue(), {
                            self.showWeatherInfo(currentTemp)
                    })
                    
                })
                
            }
        }
        task.resume()
    }
}