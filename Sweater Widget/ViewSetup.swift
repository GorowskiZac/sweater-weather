//
//  ViewSetup.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 3/3/16.
//  Copyright © 2016 Tetragon. All rights reserved.
//

import Foundation
import UIKit
import SweaterKit

extension TodayViewController {
    func showLocationDenied() {
        self.widgetMessage.text = "Location Access Denied"
        self.widgetCondition.text = "You've denied access to your location."
    }
    
    func showNetworkFailure() {
        self.widgetMessage.text = "Poor Data Connection"
        self.widgetCondition.text = "Your network connection is poor."
    }
    
    func showWeatherInfo(temp: Double) {
        self.widgetMessage.text = ParseTemp(temp: temp).messageText
        self.widgetCondition.text = ParseTemp(temp: temp).conditionText
    }
}
