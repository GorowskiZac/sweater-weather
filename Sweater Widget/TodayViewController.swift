//
//  TodayViewController.swift
//  Sweater Widget
//
//  Created by Zac Gorowski on 11/1/15.
//  Copyright © 2015 Tetragon. All rights reserved.
//

import UIKit
import NotificationCenter
import MapKit
import SweaterKit

class TodayViewController: UIViewController, NCWidgetProviding, CLLocationManagerDelegate {
    
    // MARK: Outlets
    @IBOutlet weak var widgetMessage: UILabel!
    @IBOutlet weak var widgetCondition: UILabel!
    
    // MARK: API Values
    var myData = NSMutableData()
    var callGuard = 0
    
    /* Location Items */
    var userLatLngArray : [Double] = []
    let locationMngr = CLLocationManager()
    
    // MARK: View Setup
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* Set Content Size */
        preferredContentSize.height = 90
        
        /* Check Network */
        if Reachability().isConnected == true {
            /* Setup Location Manager */
            locationMngr.delegate = self
            locationMngr.requestAlwaysAuthorization()
        } else {
            showNetworkFailure()
        }
        
        /* Protects API Usage */
        callGuard = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)) {

        completionHandler(NCUpdateResult.NewData)
    }
    
    func widgetMarginInsetsForProposedMarginInsets(defaultMarginInsets: UIEdgeInsets) -> UIEdgeInsets {
        return UIEdgeInsetsZero
    }
    
}
