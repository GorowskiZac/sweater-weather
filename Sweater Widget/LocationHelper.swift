//
//  LocationHelper.swift
//  Sweater Weather
//
//  Created by Zac Gorowski on 11/1/15.
//  Copyright © 2015 Tetragon. All rights reserved.
//

import Foundation
import MapKit

extension TodayViewController {
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Location ERROR:  \(error)")
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.AuthorizedAlways {
            // Set Location Accurace //
            locationMngr.desiredAccuracy = kCLLocationAccuracyHundredMeters
            // Begin Updating Location //
            locationMngr.startUpdatingLocation()
            
            print("Location Authorization AUTHORIZED")
        } else {
            showLocationDenied()
            print("Location Authorization DENIED")
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLat = manager.location!.coordinate.latitude
        let userLng = manager.location!.coordinate.longitude
        
        userLatLngArray = [userLat as Double, userLng as Double]
        print(userLatLngArray)
        
        if callGuard < 1 {
            /* Set URL */
            let url = NSURL(string: "https://api.forecast.io/forecast/e308e7efb0cebaad4be3b473acc2e380/\(Double(userLat)),\(Double(userLng))")
            /* Call API */
            let request = NSMutableURLRequest(URL: url!)
            httpGet(request)
        }
        callGuard++
        locationMngr.stopUpdatingLocation()
    }
    
}