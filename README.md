# README #

Sweater Weather is a weather application based on the love of simplicity and minimalism.    

This application is written in Apple's Swift language using Xcode, and will be updated regularly to improve design, performance, compatibility.    

Device Compatibility: iPhone 4, iPhone 4S, iPhone 5, iPhone 5S/C, iPhone 6, iPhone 6S iPhone 6+, iPhone 6S+